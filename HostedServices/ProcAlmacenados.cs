﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaBackend.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaBackend.HostedServices
{
    public class ProcAlmacenados : IHostedService, IDisposable
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ILogger<ProcAlmacenados> _logger;
        private Timer? _timer;
        public ProcAlmacenados(IServiceScopeFactory scopeFactory, ILogger<ProcAlmacenados> logger)
        {
            _scopeFactory = scopeFactory;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted service facturador iniciando.");
            _timer = new Timer(ProcessAlmacenados, null, TimeSpan.FromSeconds(0), TimeSpan.FromMinutes(1));
            return Task.CompletedTask;
        }
        private async void ProcessAlmacenados(object? state)
        {
            using var ambito = _scopeFactory.CreateScope();
            var _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
            // Poner todos los procedimientos almacenados pesados
            _ = await _context.Database.ExecuteSqlRawAsync("EXECUTE [dbo].[facProcedimientoGeneral]");
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted Service facturador deteniendose.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _timer?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
