﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaBackend.Data;
using SofiaBackend.Models;
using SofiaBackend.Services;
using SofiaBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaBackend.HostedServices
{
    /// <summary>
    /// Hosted Service "Facturador", esta clase es singleton
    /// </summary>
    public class Facturador : IHostedService, IDisposable
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly LibelulaApi _libelulaApi;
        private readonly ITestInternet _internet;
        private readonly ILogger<Facturador> _logger;
        private Timer? _timer;
        public Facturador(IServiceScopeFactory serviceScopeFactory, LibelulaApi libelulaApi, ITestInternet internet, ILogger<Facturador> logger)
        {
            _scopeFactory = serviceScopeFactory;
            _libelulaApi = libelulaApi;
            _internet = internet;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted service facturador iniciando.");
            _timer = new Timer(Facturar, null, TimeSpan.FromSeconds(10), TimeSpan.FromMinutes(2));
            return Task.CompletedTask;
        }
        private async void Facturar(object? state)
        {
            int contador = 1;
            int numeroLote = 10;
            string? Appkey;
            string? TokenSiat;
            _logger.LogInformation("Facturacion en ejecucion");
            bool ExisteInternet = await _internet.ExisteInternet();
            if (!ExisteInternet)
            {
                _logger.LogInformation("La conexion a internet no esta activa");
                return;
            }
            using (var ambito = _scopeFactory.CreateScope())
            {
                var _siat = ambito.ServiceProvider.GetRequiredService<ISiatFacturacion>();
                var _data = ambito.ServiceProvider.GetRequiredService<IDataFacturacion>();
                var _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
                // Siat (Impuestos Nacionales)
                TokenSiat = await (from c in _context.FacConfiguraciones where c.Estado select c.TokenDelegado).AsNoTracking().FirstOrDefaultAsync();
                bool siat_online = await _siat.VerificarComunicacionSiat(TokenSiat);
                if (!siat_online)
                {
                    _logger.LogInformation("Los servicios de Impuestos Nacionales no estan activos");
                    return;
                }
                // Libelula
                Appkey = await (from c in _context.FacConfiguraciones where c.Estado select c.AppKey).AsNoTracking().FirstOrDefaultAsync();
                bool libelula_online = await _libelulaApi.EsOnline(Appkey);
                if (!libelula_online)
                {
                    _logger.LogInformation("Los servicios de Libelula/Todotix no estan activos");
                    return;
                }
                // Cargar datos
                IEnumerable<Factura> facturasEmision = await _data.GetAllData();
                foreach (Factura item in facturasEmision)
                {
                    FacturaPagoCaja? resultado = await _libelulaApi.RegistrarFactura(item);
                    if (resultado == null)
                    {
                        // Registro de la respuesta nula de Libelula
                        FacRegistroEvento eventoNull = new()
                        {
                            IdFactura = item.IdFactura,
                            Error = 1,
                            Existente = 0,
                            Mensaje = "Respuesta nula de Libelula",
                            IdTransaccion = "",
                            UrlPasarelaPagos = ""
                        };
                        _ = await _context.FacRegistroEventos.AddAsync(eventoNull);
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 16");
                        contador++;
                        continue;
                    }
                    // Registro de la respuesta de Libelula
                    FacRegistroEvento evento = new()
                    {
                        IdFactura = item.IdFactura,
                        Error = resultado.Error,
                        Existente = resultado.Existente,
                        Mensaje = resultado.Mensaje,
                        IdTransaccion = resultado.IdTransaccion,
                        UrlPasarelaPagos = resultado.UrlPasarelaPagos
                    };
                    _ = await _context.FacRegistroEventos.AddAsync(evento);
                    // Cargado de datos de la factura electronica
                    if ((resultado.Error == 0 && resultado.Existente == 0) || (resultado.Error == 2 && resultado.Existente == 1))
                    {
                        FacturaElectronica facturaEmitida = (from f in resultado.FacturasElectronicas select f).First();
                        var cobranza_fac = await _context.AoVentasCobranzaFacs.FindAsync(item.IdFactura);
                        if (cobranza_fac != null)
                        {
                            cobranza_fac.UrlTodotix = facturaEmitida.Url;
                            cobranza_fac.Identificador = facturaEmitida.Identificador;
                            cobranza_fac.NroFactura = facturaEmitida.NumeroFactura;
                            cobranza_fac.DosificaAutorizacion = facturaEmitida.NumeroAutorizacion;
                            cobranza_fac.FechaFac = facturaEmitida.FechaPagoCanal;
                            cobranza_fac.FechaFacSin = facturaEmitida.FechaPagoCanal;
                            cobranza_fac.Cufd = facturaEmitida.Cufd;
                            cobranza_fac.UrlSinSfe = facturaEmitida.UrlSinSfe;
                            cobranza_fac.EstadoFacId = 9;
                            _context.Entry(cobranza_fac).State = EntityState.Modified;
                        }
                        if (resultado.Error == 0 && resultado.Existente == 0)
                        {
                            // Contabilizacion
                            _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[conta_fac_elec] {item.IdFactura}");
                            // Respaldo de detalle
                            _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRespaldoDetalle] {item.IdFactura}");
                        }
                    }
                    else
                    {
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 15");
                    }
                    // Guardado de cambios por lote
                    if (contador % numeroLote == 0)
                    {
                        try
                        {
                            _ = await _context.SaveChangesAsync();
                        }
                        catch (DbUpdateConcurrencyException)
                        {
                            throw;
                        }
                    }
                    contador++;
                }
                // Guardado de cambios fuera del lote
                try
                {
                    _ = await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            _logger.LogInformation("Facturacion terminada");
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted Service facturador deteniendose.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _timer?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
