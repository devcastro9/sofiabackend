﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaBackend.Data;
using SofiaBackend.Models;
using SofiaBackend.Services;
using SofiaBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaBackend.HostedServices
{
    /// <summary>
    /// Hosted Service "Anulador", esta clase es singleton
    /// </summary>
    public class Anulador : IHostedService, IDisposable
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly LibelulaApi _libelulaApi;
        private readonly ITestInternet _internet;
        private readonly ILogger<Anulador> _logger;
        private Timer? _timer;
        public Anulador(IServiceScopeFactory serviceScopeFactory, LibelulaApi libelulaApi, ITestInternet internet, ILogger<Anulador> logger)
        {
            _scopeFactory = serviceScopeFactory;
            _libelulaApi = libelulaApi;
            _internet = internet;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted service de anulacion programado iniciando.");
            _timer = new Timer(Anular, null, TimeSpan.FromSeconds(440), TimeSpan.FromMinutes(7));
            return Task.CompletedTask;
        }
        private async void Anular(object? state)
        {
            string? Appkey;
            string? TokenSiat;
            _logger.LogInformation("Anulador ejecutandose");
            bool ExisteInternet = await _internet.ExisteInternet();
            if (!ExisteInternet)
            {
                _logger.LogInformation("La conexion a internet no esta activa");
                return;
            }
            using (var ambito = _scopeFactory.CreateScope())
            {
                var _siat = ambito.ServiceProvider.GetRequiredService<ISiatFacturacion>();
                var _data = ambito.ServiceProvider.GetRequiredService<IDataAnulacion>();
                var _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
                // Siat (Impuestos nacionales)
                TokenSiat = await (from c in _context.FacConfiguraciones where c.Estado select c.TokenDelegado).AsNoTracking().FirstOrDefaultAsync();
                bool siat_online = await _siat.VerificarComunicacionSiat(TokenSiat);
                if (!siat_online)
                {
                    _logger.LogInformation("La conexion a internet no esta activa");
                    return;
                }
                // Libelula
                Appkey = await (from c in _context.FacConfiguraciones where c.Estado select c.AppKey).AsNoTracking().FirstOrDefaultAsync();
                bool libelula_online = await _libelulaApi.EsOnline(Appkey);
                if (!libelula_online)
                {
                    _logger.LogInformation("Los servicios de Libelula/Todotix no estan activos");
                    return;
                }
                // Cargar datos
                IEnumerable<AnulaFac> facturasAnulacion = await _data.GetAllAnula();
                foreach (AnulaFac item in facturasAnulacion)
                {
                    AnulaResp? resultadoAnulacion = await _libelulaApi.AnularFactura(item);
                    if (resultadoAnulacion == null)
                    {
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 19");
                        continue;
                    }
                    // Registro de la respuesta de Libelula
                    FacRegistroEvento evento = new()
                    {
                        IdFactura = item.IdFactura,
                        Error = resultadoAnulacion.Estado ? 0 : 1,
                        Existente = 0,
                        Mensaje = resultadoAnulacion.Mensaje,
                        IdTransaccion = "",
                        UrlPasarelaPagos = "ANULACION"
                    };
                    await _context.FacRegistroEventos.AddAsync(evento);
                    // Actualizacion de estado de anulacion
                    int estado_anulacion = resultadoAnulacion.Estado ? 10 : 18;
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, {estado_anulacion}");
                    // Anulacion - Reversion de comprobante contable
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[conta_fac_elec_anula] {item.IdFactura}");
                }
                // Guardado de cambios
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            _logger.LogInformation("Anulador finalizado");
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted Service de anulacion programado deteniendose.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _timer?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
