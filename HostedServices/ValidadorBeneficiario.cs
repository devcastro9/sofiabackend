﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaBackend.Data;
using SofiaBackend.Models;
using SofiaBackend.Services;
using SofiaBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaBackend.HostedServices
{
    public class ValidadorBeneficiario : IHostedService, IDisposable
    {
        private readonly IServiceScopeFactory _scopeFactory;
        public readonly Padron _padron;
        private readonly ILogger<ValidadorBeneficiario> _logger;
        private Timer? _timer;
        public ValidadorBeneficiario(IServiceScopeFactory scopeFactory, Padron padron, ILogger<ValidadorBeneficiario> logger)
        {
            _scopeFactory = scopeFactory;
            _padron = padron;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Validador de beneficiarios iniciando.");
            _timer = new Timer(Validar, null, TimeSpan.FromMinutes(5.1), TimeSpan.FromMinutes(5));
            return Task.CompletedTask;
        }
        private async void Validar(object? state)
        {
            int contador = 0;
            int numeroLote = 50;
            _logger.LogInformation("Validacion en ejecucion");
            using (var ambito = _scopeFactory.CreateScope())
            {
                var _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
                // Analisis Completo
                // _ = await _context.GcBeneficiarios.ExecuteUpdateAsync(m => m.SetProperty(p => p.Analizado, p => false));
                // Reseteo de datos SIN
                _ = await _context.Database.ExecuteSqlRawAsync(@"EXECUTE [dbo].[facValidacionNIT]");
                // Inicio de Validacion
                List<GcBeneficiario> result = await _context.GcBeneficiarios.Where(m => m.Analizado == false && m.ColaRevision == true).ToListAsync();
                foreach (var gcBeneficiario in result)
                {
                    bool valido = long.TryParse(gcBeneficiario.BeneficiarioNit, out long nit);
                    if (valido)
                    {
                        VerificacionNit? getResponse = await _padron.VerificarNit(nit);
                        if (getResponse != null)
                        {
                            // Caso de respuesta positiva
                            gcBeneficiario.BeneficiarioNit = nit.ToString();
                            if (getResponse.Ok)
                            {
                                gcBeneficiario.ValidoSin = true;
                                gcBeneficiario.RazonSocialSin = getResponse.RazonSocial;
                                gcBeneficiario.ActivoSin = getResponse.Estado == "ACTIVO";
                            }
                            else
                            {
                                gcBeneficiario.ValidoSin = false;
                                gcBeneficiario.ActivoSin = false;
                                gcBeneficiario.RazonSocialSin = (from m in getResponse.Mensajes select m.Descripcion).First();
                            }
                        }
                        else
                        {
                            gcBeneficiario.ValidoSin = false;
                            gcBeneficiario.ActivoSin = false;
                            gcBeneficiario.RazonSocialSin = "RESP_NULA";
                        }
                    }
                    else
                    {
                        gcBeneficiario.ValidoSin = false;
                        gcBeneficiario.ActivoSin = false;
                        gcBeneficiario.RazonSocialSin = "NO-VALIDO";
                    }
                    gcBeneficiario.Analizado = true;
                    _context.Entry(gcBeneficiario).State = EntityState.Modified;
                    // Ignora para el trigger
                    _context.Entry(gcBeneficiario).Property(e => e.TipodocCodigo).IsModified = false;
                    _context.Entry(gcBeneficiario).Property(e => e.BeneficiarioNit).IsModified = false;
                    // Incremento de contador
                    contador++;
                    // Guardado de cambios por lote
                    if (contador % numeroLote == 0)
                    {
                        try
                        {
                            await _context.SaveChangesAsync();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                // Guardado de cambios fuera del lote
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            _logger.LogInformation("Validador de beneficiarios terminada");
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted Service beneficiarios deteniendose.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _timer?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
