﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SofiaBackend.Utility
{
    public static class Utilitario
    {
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }
            try
            {
                // Normalizacion del dominio
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examina la parte del dominio del correo electrónico y la normaliza.
                static string DomainMapper(Match match)
                {
                    // Utilice la clase IdMapping para convertir nombres de dominio Unicode.
                    IdnMapping idn = new();

                    // Extraiga y procese el nombre de dominio (arroja ArgumentException en no válido)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }
            try
            {
                return Regex.IsMatch(email, @"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
        public static string ExtractDomain(string email)
        {
            Match result = Regex.Match(email, @"(@)(.+)$", RegexOptions.IgnoreCase);
            if (result.Success)
            {
                return result.Groups[2].Value.ToLower();
            }
            else
            {
                return "";
            }
        }
    }
}
