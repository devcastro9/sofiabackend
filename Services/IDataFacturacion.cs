﻿using SofiaBackend.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    public interface IDataFacturacion
    {
        public Task<Factura?> GetData(long id);
        public Task<IEnumerable<Factura>> GetAllData();
    }
}
