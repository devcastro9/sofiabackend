﻿using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    /// <summary>
    /// Interface para SiatFacturacion
    /// </summary>
    public interface ISiatFacturacion
    {
        public Task<bool> VerificarComunicacionSiat(string? token_siat);
    }
}
