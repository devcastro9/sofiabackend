﻿using Microsoft.EntityFrameworkCore;
using SofiaBackend.Data;
using SofiaBackend.Models;
using SofiaBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    public class DataAnulacion : IDataAnulacion
    {
        private readonly AppDbContext _context;
        public DataAnulacion(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<AnulaFac>> GetAllAnula()
        {
            List<AnulaFac> lstAnulaciones = new();
            try
            {
                List<long> idfacturas = await _context.FacturaElecAnulaciones.AsNoTracking().Where(m => m.ValidoAnulacion == true).Select(m => m.IdFactura).ToListAsync();
                foreach (var x in idfacturas)
                {
                    AnulaFac? anulacion = await GetAnula(x);
                    if (anulacion != null)
                    {
                        lstAnulaciones.Add(anulacion);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstAnulaciones;
        }

        public async Task<AnulaFac?> GetAnula(long id)
        {
            try
            {
                FacturaElecAnulacion? facAnula = await _context.FacturaElecAnulaciones.AsNoTracking().Where(m => m.IdFactura == id).FirstOrDefaultAsync();
                if (facAnula == null)
                {
                    throw new Exception();
                }
                return new AnulaFac() { AppKey = facAnula.AppKey ?? "", IdFactura = facAnula.IdFactura, Monto = facAnula.Monto ?? decimal.Zero };
            }
            catch (Exception)
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 17");
                return null;
            }
        }
    }
}
