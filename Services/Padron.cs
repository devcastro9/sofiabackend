﻿using SofiaBackend.Utility;
using SofiaBackend.ViewModels;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    /// <summary>
    /// Servicio REST de Impuestos Nacionales para la verificacion de NIT
    /// </summary>
    public class Padron
    {
        private readonly HttpClient _httpClient;
        private readonly int _retardo = 10;
        public Padron(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(@"http://pbdw.impuestos.gob.bo:8080/gob.sin.padron.servicio.web/consulta/");
        }
        /// <summary>
        /// Funcion para verificar si un NIT es valido.
        /// </summary>
        /// <param name="nit">Valor numerico del NIT</param>
        /// <returns>Retorna la respuesta en forma de objeto</returns>
        public async Task<VerificacionNit?> VerificarNit(long nit)
        {
            try
            {
                // Ejecutamos la peticion
                HttpResponseMessage response = await _httpClient.GetAsync($"verificarContribuyente?nit={nit}");
                // Retraso
                await Task.Delay(_retardo);
                // Generamos una excepcion si el codigo HTTP indica un error
                response.EnsureSuccessStatusCode();
                // Deserializamos
                JsonSerializerOptions options = new()
                {
                    ReferenceHandler = ReferenceHandler.IgnoreCycles
                };
                options.Converters.Add(new FormatDateTimeNullConverter("dd/MM/yyyy"));
                string result = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<VerificacionNit>(result, options);
            }
            catch (Exception ex)
            {
                return new VerificacionNit() { Ok = false, Nit = 0, RazonSocial = ex.Message, Estado = "Excepcion" };
            }
        }
    }
}
