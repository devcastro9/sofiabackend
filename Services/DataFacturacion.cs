﻿using Microsoft.EntityFrameworkCore;
using SofiaBackend.Data;
using SofiaBackend.Models;
using SofiaBackend.Utility;
using SofiaBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    public class DataFacturacion : IDataFacturacion
    {
        private readonly AppDbContext _context;
        private readonly Padron _padron;
        private readonly AbstractApi _abstractApi;
        private readonly ParallelOptions optionsParallel = new() { MaxDegreeOfParallelism = 2 };
        public DataFacturacion(AppDbContext context, Padron padron, AbstractApi abstractApi)
        {
            _context = context;
            _padron = padron;
            _abstractApi = abstractApi;
        }
        public async Task<Factura?> GetData(long id)
        {
            // Variables
            long nit_convertido = 0;
            int productoSin = 0;
            string productoEmpresa = "0";
            decimal costo = decimal.Zero;
            try
            {
                // Carga de datos (cabecera)
                FacturaElec? cabecera = await _context.FacturaElecs.AsNoTracking().Where(m => m.IdFactura == id).FirstOrDefaultAsync();
                List<FacturaElecDetalle> detalles = await _context.FacturaElecDetalleHomologado(id).AsNoTracking().ToListAsync();
                //List <FacturaElecDetalle> detalles = await _context.FacturaElecDetalles.AsNoTracking().Where(m => m.IdFactura == id).ToListAsync();
                /*
                 * -------------------------------------------------------------------
                 * Datos no validos desde las vistas almacenadas o problema de sistema
                 * -------------------------------------------------------------------
                 */
                if (cabecera == null || detalles.Count < 1)
                {
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 14");
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRegistraEvento] {id}, 14");
                    return null;
                }
                /*
                 * ------------------
                 * Validacion de NIT
                 * ------------------
                 */
                if (cabecera.TipoDoc == "NIT" && cabecera.ValidarNit)
                {
                    if (!long.TryParse(cabecera.Nit, out nit_convertido))
                    {
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 12");
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRegistraEvento] {id}, 12");
                        return null;
                    }
                    if (nit_convertido > 0)
                    {
                        VerificacionNit? resultValidarNit = await _padron.VerificarNit(nit_convertido);
                        if (resultValidarNit != null && resultValidarNit.Ok)
                        {
                            // Sobreescritura de NIT
                            cabecera.Nit = resultValidarNit.Nit.ToString();
                            // Sobreescritura de razon social
                            cabecera.RazonSocial = resultValidarNit.RazonSocial;
                        }
                        else
                        {
                            _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 12");
                            _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRegistraEvento] {id}, 12");
                            return null;
                        }
                    }
                    else
                    {
                        cabecera.Nit = nit_convertido.ToString();
                    }
                }
                /*
                 * --------------------
                 * Validacion de Email
                 * --------------------
                 */
                if (cabecera.ValidarEmail)
                {
                    // Cambio de estado
                    if (cabecera.Email == null || !Utilitario.IsValidEmail(cabecera.Email))
                    {
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 13");
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRegistraEvento] {id}, 13");
                        return null;
                    }
                    // Validacion Abstract Api
                    string ApiKey = await (from c in _context.FacKeys where c.IdKey == 1 select c.Appkey).AsNoTracking().FirstAsync();
                    AbstractEmail? resultValidarEmail = await _abstractApi.ValidarEmail(ApiKey, cabecera.Email);
                    // Sin respuesta (JSON en NULL)
                    if (resultValidarEmail == null)
                    {
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 13");
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRegistraEvento] {id}, 13");
                        return null;
                    }
                    // Analisis de la respuesta
                    if ((resultValidarEmail.EsSmtpValido.Valor != null && resultValidarEmail.EsSmtpValido.Valor == true && resultValidarEmail.Entregable == "DELIVERABLE") || resultValidarEmail.Puntuacion >= 0.7m)
                    {
                        // Re-asignacion de email
                        cabecera.Email = resultValidarEmail.Email;
                    }
                    else
                    {
                        // Dominio en listas negras
                        var EmailDominio = await _context.FacDominioEmails.AsNoTracking().Where(m => m.DominioIgnorado == Utilitario.ExtractDomain(resultValidarEmail.Email) && m.Habilitado == true).FirstOrDefaultAsync();
                        if (EmailDominio == null)
                        {
                            _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 13");
                            _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRegistraEvento] {id}, 13");
                            return null;
                        }
                    }
                    //if (cabecera.EmailCobr == null || cabecera.EmailCobr.Length < 3)
                    //{
                    //    // No existe un email del cobrador, por lo que no es posible redireccionar el email en caso de error
                    //    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 13");
                    //    return null;
                    //}
                    // Redireccion a Email
                    //if (cabecera.Email != null && cabecera.Email.Length > 3)
                    //{
                    //    string ApiKey = await (from c in _context.FacKeys where c.IdKey == 1 select c.Appkey).AsNoTracking().FirstAsync();
                    //    AbstractEmail? resultValidarEmail = await _abstractApi.ValidarEmail(ApiKey, cabecera.Email);
                    //    // Plan basico
                    //    //await Task.Delay(1000);
                    //    // Plan premium
                    //    await Task.Delay(100);
                    //    if (resultValidarEmail != null && resultValidarEmail.EsSmtpValido.Valor && resultValidarEmail.Entregable == "DELIVERABLE")
                    //    {
                    //        cabecera.Email = resultValidarEmail.Email;
                    //    }
                    //    else
                    //    {
                    //        cabecera.Email = cabecera.EmailCobr;
                    //    }
                    //}
                    //else
                    //{
                    //    cabecera.Email = cabecera.EmailCobr;
                    //}
                }
                /*
                 * -----------------
                 * Carga de cabecera
                 * -----------------
                 */
                Factura factura = new()
                {
                    AppKey = cabecera.AppKey ?? "",
                    IdFactura = id,
                    EmailCliente = cabecera.Email ?? "otisbolivia@gmail.com",
                    CallbackUrl = cabecera.Curl + id,
                    Descripcion = cabecera.FacturaNota ?? "",
                    RazonSocial = cabecera.RazonSocial ?? "S/N",
                    NumeroDocumento = cabecera.Nit ?? "0",
                    CodigoTipoDocumento = cabecera.TipoDoc ?? "NIT",
                    CodigoCliente = cabecera.CodigoCliente.ToString(),
                    FacturaNotaCliente = cabecera.FacturaNota ?? "Factura electronica sin descripcion",
                    TipoFactura = cabecera.TipoFactura,
                    EmiteFactura = cabecera.EmiteFactura != 0,
                    Moneda = cabecera.TipoMoneda,
                    TipoCambio = cabecera.CambioOficial,
                    CanalCaja = cabecera.CanalCaja,
                    CanalCajaSucursal = cabecera.CanalCajaSucursal,
                    CanalCajaUsuario = "Facturacion: " + cabecera.CanalCajaUsuario
                };
                /*
                 * -----------------
                 * Carga de detalles
                 * -----------------
                 */
                _ = Parallel.ForEach(detalles, optionsParallel, item =>
                {
                    productoSin = item.ProductoSin ?? 0;
                    productoEmpresa = item.Producto ?? "";
                    costo = item.Costo ?? costo;
                    if (productoSin > 0 && productoEmpresa.Length > 0 && costo > 0)
                    {
                        FacturaDetalle detalle = new()
                        {
                            ActividadEconomica = item.Actividad,
                            CodigoProductoSin = productoSin,
                            CodigoProducto = productoEmpresa,
                            Concepto = item.Concepto ?? "",
                            Cantidad = item.Cantidad,
                            UnidadMedida = item.UnidadMedida,
                            CostoUnitario = costo
                        };
                        factura.FacturaDetalles.Add(detalle);
                    }
                });
                /*
                 * ------------------
                 * Carga de Metadatos
                 * ------------------
                 */
                List<FacturaElecMetadato> metadatos = await _context.FacturaElecMetadatos.AsNoTracking().Where(m => m.IdFactura == id).ToListAsync();
                if (metadatos.Count > 0)
                {
                    _ = Parallel.ForEach(metadatos, optionsParallel, item =>
                    {
                        if (item.Nombre != null && item.Dato != null)
                        {
                            FacturaMetadato metadato = new()
                            {
                                Nombre = item.Nombre,
                                Dato = item.Dato
                            };
                            factura.FacturaMetadatos.Add(metadato);
                        }
                    });
                }
                return factura;
            }
            catch (Exception)
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {id}, 16");
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRegistraEvento] {id}, 16");
                return null;
            }
        }
        public async Task<IEnumerable<Factura>> GetAllData()
        {
            // Variables
            List<Factura> lstFactura = new();
            try
            {
                // Consulta a la base de datos
                List<long> idfacturas = await _context.FacturaElecs.AsNoTracking().Select(m => m.IdFactura).ToListAsync();
                // Creacion de la lista de Facturas
                foreach (long x in idfacturas)
                {
                    Factura? objFac = await GetData(x);
                    if (objFac != null)
                    {
                        lstFactura.Add(objFac);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstFactura;
        }
    }
}
