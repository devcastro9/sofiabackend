﻿using SofiaBackend.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    public interface IDataAnulacion
    {
        public Task<AnulaFac?> GetAnula(long id);
        public Task<IEnumerable<AnulaFac>> GetAllAnula();
    }
}
