﻿using System;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    /// <summary>
    /// Servicio para realizar un test para determinar la existencia de internet
    /// </summary>
    public class TestInternet : ITestInternet, IDisposable
    {
        private readonly string PrimaryUrl = @"8.8.8.8";
        private readonly string SecondaryUrl = @"1.1.1.1";
        private Ping? _ping;
        /// <summary>
        /// Funcion para verificar la existencia de conectividad a internet
        /// </summary>
        /// <returns>Un valor booleano que representa la conectividad</returns>
        public async Task<bool> ExisteInternet()
        {
            try
            {
                _ping = new Ping();
                PingReply result = await _ping.SendPingAsync(PrimaryUrl);
                if (result.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    result = await _ping.SendPingAsync(SecondaryUrl);
                    return result.Status == IPStatus.Success;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void Dispose()
        {
            _ping?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
