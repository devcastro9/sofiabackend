﻿using System.Threading.Tasks;

namespace SofiaBackend.Services
{
    /// <summary>
    /// Interface para TestInternet
    /// </summary>
    public interface ITestInternet
    {
        public Task<bool> ExisteInternet();
    }
}
