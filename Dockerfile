FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
ENV TZ=America/La_Paz
ENV ASPNETCORE_URLS=http://+:5000
ENV TERM=xterm-256color
WORKDIR /app
EXPOSE 5000
#EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["SofiaBackend.csproj", "."]
RUN dotnet restore "./SofiaBackend.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "SofiaBackend.csproj" -c Release -o /app/build --nologo

FROM build AS publish
RUN dotnet publish "SofiaBackend.csproj" -c Release -o /app/publish /p:UseAppHost=false --nologo

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SofiaBackend.dll"]