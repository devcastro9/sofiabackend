﻿using Microsoft.EntityFrameworkCore;
using SofiaBackend.Models;
using System.Linq;

namespace SofiaBackend.Data;

public partial class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<AoVentasCobranzaFac> AoVentasCobranzaFacs { get; set; }

    public virtual DbSet<FacConfiguracion> FacConfiguraciones { get; set; }

    public virtual DbSet<FacDominioEmail> FacDominioEmails { get; set; }

    public virtual DbSet<FacEstado> FacEstados { get; set; }

    public virtual DbSet<FacEstadoTipo> FacEstadoTipos { get; set; }

    public virtual DbSet<FacKey> FacKeys { get; set; }

    public virtual DbSet<FacRegistroEvento> FacRegistroEventos { get; set; }

    public virtual DbSet<FacturaElec> FacturaElecs { get; set; }

    public virtual DbSet<FacturaElecAnulacion> FacturaElecAnulaciones { get; set; }

    public virtual DbSet<FacturaElecDetalle> FacturaElecDetalles { get; set; }

    public virtual DbSet<FacturaElecMetadato> FacturaElecMetadatos { get; set; }

    public virtual DbSet<GcBeneficiario> GcBeneficiarios { get; set; }
    public IQueryable<FacturaElecDetalle> FacturaElecDetalleHomologado(long IdFactura) => FromExpression(() => FacturaElecDetalleHomologado(IdFactura));
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Metodo base
        base.OnModelCreating(modelBuilder);
        // Api fluent
#pragma warning disable CS8604 // Posible argumento de referencia nulo
        modelBuilder.HasDbFunction(typeof(AppDbContext).GetMethod(nameof(FacturaElecDetalleHomologado), new[] { typeof(long) }))
            .HasName("factura_detalle_homologacion");
#pragma warning restore CS8604 // Posible argumento de referencia nulo
        // Tables and views
        modelBuilder.Entity<AoVentasCobranzaFac>(entity =>
        {
            entity.Property(e => e.CodigoEmpresa).HasDefaultValueSql("((0))");
            entity.Property(e => e.DebitoFiscal13Bs).HasDefaultValueSql("((0))");
            entity.Property(e => e.DebitoFiscal13Dol).HasDefaultValueSql("((0))");
            entity.Property(e => e.DosificaAutorizacion).HasDefaultValueSql("((0))");
            entity.Property(e => e.EstadoCobrado)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoCodigo)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoCodigoFac)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoFac)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.Factura87Bs).HasDefaultValueSql("((0))");
            entity.Property(e => e.Factura87Dol).HasDefaultValueSql("((0))");
            entity.Property(e => e.FacturaImpresa).IsFixedLength();
            entity.Property(e => e.NroFactura).HasDefaultValueSql("((0))");
            entity.Property(e => e.SolicitudTipo).HasDefaultValueSql("((0))");
            entity.Property(e => e.TipoBienServicio).HasDefaultValueSql("((0))");
        });

        modelBuilder.Entity<FacConfiguracion>(entity =>
        {
            entity.Property(e => e.DtLastUpdateDt).HasDefaultValueSql("(getdate())");
        });

        modelBuilder.Entity<FacDominioEmail>(entity =>
        {
            entity.Property(e => e.Habilitado).HasDefaultValueSql("((0))");
        });

        modelBuilder.Entity<FacEstado>(entity =>
        {
            entity.Property(e => e.EsModificable).HasDefaultValueSql("((0))");
            entity.Property(e => e.Habilitado).HasDefaultValueSql("((0))");

            entity.HasOne(d => d.TipoFac).WithMany(p => p.FacEstados).HasConstraintName("FK_facEstado_facEstadoTipo");
        });

        modelBuilder.Entity<FacRegistroEvento>(entity =>
        {
            entity.Property(e => e.DtCreationDt).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.Error).HasDefaultValueSql("((1))");
        });

        modelBuilder.Entity<FacturaElec>(entity =>
        {
            entity.ToView("FacturaElec");

            entity.Property(e => e.EstadoCodigoFac).IsFixedLength();
        });

        modelBuilder.Entity<FacturaElecAnulacion>(entity =>
        {
            entity.ToView("FacturaElecAnulacion");
        });

        modelBuilder.Entity<FacturaElecDetalle>(entity =>
        {
            entity.ToView("FacturaElecDetalle");
        });

        modelBuilder.Entity<FacturaElecMetadato>(entity =>
        {
            entity.ToView("FacturaElecMetadato");
        });

        modelBuilder.Entity<GcBeneficiario>(entity =>
        {
            entity.ToTable("gc_beneficiario", tb => tb.HasTrigger("trigger_actualiza_beneficiario"));

            entity.Property(e => e.ActivoSin).HasDefaultValueSql("((0))");
            entity.Property(e => e.Analizado).HasDefaultValueSql("((0))");
            entity.Property(e => e.BeneficiarioDeudor).IsFixedLength();
            entity.Property(e => e.ColaRevision).HasDefaultValueSql("((0))");
            entity.Property(e => e.EstadoCodigo)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoCodigoContrato)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.RazonSocialSin).HasDefaultValueSql("('-')");
            entity.Property(e => e.ValidoSin).HasDefaultValueSql("((0))");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
