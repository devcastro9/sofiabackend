﻿using System.Text.Json.Serialization;

namespace SofiaBackend.ViewModels
{
    public class FacturaMetadato
    {
        [JsonPropertyName("nombre")]
        public string Nombre { get; set; } = null!;
        [JsonPropertyName("dato")]
        public string Dato { get; set; } = null!;
    }
}
