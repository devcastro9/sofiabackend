using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SofiaBackend.ViewModels
{
    public class FacturaPagoCaja
    {
        public FacturaPagoCaja()
        {
            FacturasElectronicas = new HashSet<FacturaElectronica>();
        }
        [JsonPropertyName("error")]
        public int Error { get; set; }
        [JsonPropertyName("existente")]
        public int Existente { get; set; }
        [JsonPropertyName("mensaje")]
        public string? Mensaje { get; set; }
        [JsonPropertyName("codigo_recaudacion")]
        public string? CodigoRecaudacion { get; set; }
        [JsonPropertyName("id_transaccion")]
        public string? IdTransaccion { get; set; }
        [JsonPropertyName("facturas_electronicas")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public virtual ICollection<FacturaElectronica> FacturasElectronicas { get; set; }
        [JsonPropertyName("url_pasarela_pagos")]
        public string? UrlPasarelaPagos { get; set; }
    }
}
