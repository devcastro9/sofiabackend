﻿using System.Text.Json.Serialization;

namespace SofiaBackend.ViewModels
{
    public class AnulaResp
    {
        [JsonPropertyName("proceso_exitoso")]
        public bool Estado { get; set; }
        [JsonPropertyName("mensaje")]
        public string Mensaje { get; set; } = null!;
    }
}
