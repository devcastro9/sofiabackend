﻿using System.Text.Json.Serialization;

namespace SofiaBackend.ViewModels
{
    public class AbstractAux
    {
        [JsonPropertyName("value")]
        public bool? Valor { get; set; }
        [JsonPropertyName("text")]
        public string? Texto { get; set; }
    }
}
