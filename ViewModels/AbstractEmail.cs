﻿using System.Text.Json.Serialization;

namespace SofiaBackend.ViewModels
{
    public class AbstractEmail
    {
        [JsonPropertyName("email")]
        public string Email { get; set; } = null!;
        [JsonPropertyName("autocorrect")]
        public string AutoCorregido { get; set; } = null!;
        [JsonPropertyName("deliverability")]
        public string Entregable { get; set; } = null!;
        [JsonPropertyName("quality_score")]
        public decimal Puntuacion { get; set; }
        [JsonPropertyName("is_valid_format")]
        public virtual AbstractAux EsFormatoValido { get; set; } = null!;
        [JsonPropertyName("is_free_email")]
        public virtual AbstractAux EsEmailGratis { get; set; } = null!;
        [JsonPropertyName("is_disposable_email")]
        public virtual AbstractAux EsEmailDesechable { get; set; } = null!;
        [JsonPropertyName("is_role_email")]
        public virtual AbstractAux EsEmailRol { get; set; } = null!;
        [JsonPropertyName("is_catchall_email")]
        public virtual AbstractAux EsEmailRecepcionaTodo { get; set; } = null!;
        [JsonPropertyName("is_mx_found")]
        public virtual AbstractAux EsEmailMX { get; set; } = null!;
        [JsonPropertyName("is_smtp_valid")]
        public virtual AbstractAux EsSmtpValido { get; set; } = null!;
    }
}
