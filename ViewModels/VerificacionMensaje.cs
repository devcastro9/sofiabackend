﻿using System.Text.Json.Serialization;

namespace SofiaBackend.ViewModels
{
    public class VerificacionMensaje
    {
        [JsonPropertyName("codigo")]
        public string? Codigo { get; set; }
        [JsonPropertyName("descripcion")]
        public string? Descripcion { get; set; }
        [JsonPropertyName("tipo")]
        public string? Tipo { get; set; }
    }
}
