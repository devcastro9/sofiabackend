using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SofiaBackend.Data;
using SofiaBackend.HostedServices;
using SofiaBackend.Services;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SofiaBackend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
            // Adicion de servicios al contenedor IServiceCollection
            // Base de datos
            builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("Bdatos"), sqlOptions =>
            {
                sqlOptions.CommandTimeout(25);
                sqlOptions.EnableRetryOnFailure(maxRetryCount: 6, maxRetryDelay: TimeSpan.FromSeconds(20), errorNumbersToAdd: new List<int> { 4060 });
            }));
            // Servicios HttpClient tipados con responsabilidad unica
            // (Evitar instanciar otros HttpClient dentro de cualquier lugar, puede ocasionar el agotamiento y degradacion de los sockets - Excepcion: SocketException) 
            builder.Services.AddHttpClient<Padron>();
            builder.Services.AddHttpClient<AbstractApi>();
            builder.Services.AddHttpClient<LibelulaApi>();
            // Servicios para Inyeccion de Dependencias con ambito
            builder.Services.AddSingleton<ITestInternet, TestInternet>();
            builder.Services.AddScoped<ISiatFacturacion, SiatFacturacion>();
            builder.Services.AddScoped<IDataFacturacion, DataFacturacion>();
            builder.Services.AddScoped<IDataAnulacion, DataAnulacion>();
            // Hosted Service - Proceso en segundo plano
            builder.Services.AddHostedService<ProcAlmacenados>();
            builder.Services.AddHostedService<Facturador>();
            //builder.Services.AddHostedService<ValidadorBeneficiario>();
            builder.Services.AddHostedService<Anulador>();
            // Controladores
            builder.Services.AddControllers()
                            .AddJsonOptions(options => options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            // Configuracion Swagger/OpenAPI
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "REST API",
                    Description = "Se documentan todas las endpoints del backend"
                });
            });
            // Configuracion de CORS
            builder.Services.AddCors(options => options.AddDefaultPolicy(policy =>
            {
                policy.AllowAnyHeader()
                      .AllowAnyOrigin()
                      .AllowAnyMethod();
            }));
            // Configuracion como servicio de Windows (Para evitar el reciclaje del IIS)
            //builder.Host.UseWindowsServices();
            // Construye la aplicacion configurada
            WebApplication app = builder.Build();

            // Configuracion de la canalizaci�n de solicitudes HTTP.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            app.UseCors();
            //app.UseHttpsRedirection();

            //app.UseAuthorization();
            //app.UseAuthentication();

            app.MapControllers();

            app.Run();
        }
    }
}