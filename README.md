# Backend del sistema Sofia
Este es el backend del sistema Sofia
## Stack de desarrollo web
El stack usado en el desarrollo del sistema web.

    1. BackEnd:

        * Lenguaje de programacion backend: C# 11

        * SDK: .NET7 v7.0.100 (version LTS vigente hasta el 14 de mayo de 2024)

        * ORM: Entity Framework Core v7.0.0 (Vigente hasta el 14 de mayo de 2024)

        * Base de datos: Microsoft SQL Server 2019

    2. FrontEnd

        * Manejador de versiones de NodeJS: NVM

        * NodeJS Gallium v16.17.0 (Version LTS vigente hasta el 23 de septiembre de 2023)

        * NPM v8.15.0
        
        * Angular CLI: ng v14.1.2

        * Angular v14.1.2

        * Angular Material v14.1.2

        * Librerias Adicionales: ChartJS v3.8.0 (ng2-charts), IntroJS v5.1.0 (angular-intro.js), SweetAlert2 v11.4.20 (ngx-sweetalert2)

    3. Infraestructura

        * Windows Server 2019 (Sistema operativo del Servidor)

        * Servidor web: IIS v10 (Servidor web con proxy inverso, en caso de despliegue con Docker)

        * Docker v20 (Contenedores)

## Tiempos
Tablas de tiempos:
```
Proc:   3 min
Fac:    5 min
Benef:  7 min
Anl:    11 min
```
## Comandos para despliegue con contenedores
Construccion del contenedor Docker:
```
sudo docker build -t backend-sofia -f Dockerfile .
```
Ejecutar el contenedor:
```
sudo docker run -d -p 5000:80 -p 5001:443 --name otisapp backend-sofia
```
## Comando para ver ultimo registro de evento de las facturas
Para ver los rechazados por Libelula:
```
SELECT f.IdFactura, e.Mensaje, e.dtCreation_dt AS Fecha
FROM dbo.ao_ventas_cobranza_fac_ONLINE AS f
INNER JOIN (SELECT *
, Dultimo = ROW_NUMBER() OVER (
              PARTITION BY IdFactura
              ORDER BY dtCreation_dt DESC
            )
FROM dbo.facRegistroEvento) AS e
ON f.IdFactura = e.IdFactura
WHERE f.EstadoFacId = 15 AND e.Dultimo = 1
ORDER BY Fecha DESC
```
Ver estados vs cantidad:
```
SELECT estado_codigo_fac, EstadoFacId, COUNT(*)
FROM dbo.ao_ventas_cobranza_fac_ONLINE
GROUP BY estado_codigo_fac, EstadoFacId
ORDER BY estado_codigo_fac, EstadoFacId
```
## Comando de Mapeado de las bases de datos
PM:
Terminal:
```
dotnet --version
```
```
Scaffold-DbContext "Server=tcp:192.168.3.218,1433;Database=PRUEBA;Trusted_Connection=True;Connection Timeout=10" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables facConfiguracion,ao_ventas_cobranza_fac,
```
Valido:
```
Scaffold-DbContext "Server=tcp:192.168.3.133,1433;Database=ADMIN_EMPRESA;Trusted_Connection=True;Encrypt=False" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables facConfiguracion,facDominioEmail,facEstado,facEstadoTipo,facKey,facRegistroEvento,ao_ventas_cobranza_fac,gc_beneficiario,FacturaElec,FacturaElecDetalle,FacturaElecMetadato,FacturaElecAnulacion -Force
```
Other
```
Scaffold-DbContext "Server=tcp:192.168.3.133,1433;Database=ADMIN_EMPRESA;Trusted_Connection=True;Encrypt=False" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables facConfiguracion,facEstado,facEstadoTipo,facKey,facRegistroEvento,ao_ventas_cobranza_fac_ONLINE,FacturaElec,FacturaElecDetalle,FacturaElecMetadato,FacturaElecAnulacion -Force
```
Terminal:
```
dotnet --version
```
Probable problema con IIS y el segundo plano
Reciclaje del grupo de aplicaciones cada 20 minutos

Solucion #1: Hacerlo Servicio de Windows
https://docs.microsoft.com/en-us/dotnet/core/extensions/windows-service
https://docs.microsoft.com/en-us/dotnet/architecture/microservices/multi-container-microservice-net-applications/background-tasks-with-ihostedservice
Solucion #2: Proxy Inverso IIS + Kestrel
Solucion #3: Reconfigurar IIS para evitar el reciclado
-- Archivos
C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\PRUEBA.mdf
C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\PRUEBA_log.ldf

FacturaElec
```
SELECT TOP (100) PERCENT conf.AppKey, c.IdFactura, dbo.EsEmailValido(c.beneficiario_email) AS email, c.beneficiario_RazonSocial AS razon_social, c.beneficiario_nit AS nit, ISNULL(c.tipodoc_codigo, 'NIT') AS tipo_doc, 
                  c.edif_codigo_corto AS codigo_cliente, c.glosa_Descripcion AS factura_nota, '0' AS tipo_factura, 1 AS emite_factura, c.cambio_oficial, c.tipo_moneda, conf.CanalCaja, cab.depto_codigo, c.estado_codigo_fac
FROM     dbo.facConfiguracion AS conf INNER JOIN
                  dbo.ao_ventas_cabecera AS cab ON conf.EmpresaId = cab.codigo_empresa INNER JOIN
                  dbo.ao_ventas_cobranza_fac AS c ON cab.venta_codigo = c.venta_codigo INNER JOIN
                  dbo.facEstado AS e ON c.EstadoFacId = e.EstadoFacId INNER JOIN
                  dbo.facEstadoTipo AS t ON e.TipoFacId = t.TipoFacId
WHERE  (t.EsCola = 1) AND (e.Habilitado = 1) AND (conf.HabilitadoFacturacion = 1)

```
```
SELECT conf.AppKey, c.IdFactura, c.beneficiario_email AS email, c.beneficiario_RazonSocial AS razon_social, c.beneficiario_nit AS nit, c.edif_codigo_corto AS codigo_cliente, c.tipodoc_codigo, c.glosa_Descripcion AS factura_nota, 
                  '0' AS tipo_factura, 1 AS emite_factura, c.cambio_oficial, c.tipo_moneda, c.total_bs, c.total_dol, c.estado_codigo_fac, conf.HabilitadoFacturacion
FROM     dbo.facConfiguracion AS conf INNER JOIN
                  dbo.ao_ventas_cabecera AS cab ON conf.EmpresaId = cab.codigo_empresa INNER JOIN
                  dbo.ao_ventas_cobranza_fac AS c ON cab.venta_codigo = c.venta_codigo INNER JOIN
                  dbo.facEstado AS e ON c.EstadoFacId = e.EstadoFacId INNER JOIN
                  dbo.facEstadoTipo AS t ON e.TipoFacId = t.TipoFacId
WHERE  (t.EsCola = 1) AND (e.Habilitado = 1) AND (conf.HabilitadoFacturacion = 1)
```
FacturaElecDetalle
```
SELECT fac.IdFactura, p.CodActividad, p.CodProducto, d.cobranza_observaciones AS concepto, 1 AS Cantidad, 58 AS UnidadMedida, dbo.costo_unitario(fac.tipo_moneda, d.cobranza_total_bs, d.cobranza_total_dol) AS costo
FROM     dbo.ao_ventas_cobranza_fac2 AS fac INNER JOIN
                  dbo.ao_ventas_cobranza AS d ON fac.IdFactura = d.venta_codigo_new INNER JOIN
                  dbo.ao_ventas_cabecera AS c ON d.venta_codigo = c.venta_codigo INNER JOIN
                  dbo.gc_tipo_transaccion AS t ON c.trans_codigo = t.trans_codigo INNER JOIN
                  dbo.gc_productos_sin AS p ON t.CorrelativoSIN = p.correlativo

SELECT d.venta_codigo_new AS FacturaId, p.CodActividad, p.CodProducto, d.cobranza_observaciones AS concepto, 1 AS Cantidad, 58 AS UnidadMedida, d.cobranza_total_bs AS bob, d.cobranza_total_dol AS usd
FROM     dbo.ao_ventas_cobranza AS d INNER JOIN
                  dbo.ao_ventas_cabecera AS c ON d.venta_codigo = c.venta_codigo INNER JOIN
                  dbo.gc_tipo_transaccion AS t ON c.trans_codigo = t.trans_codigo INNER JOIN
                  dbo.gc_productos_sin AS p ON t.CorrelativoSIN = p.correlativo
```
facRegistroRespuesta

CI
CEX
PSP
OD
NIT

Ya facturados
(83378, 83388, 83379, 83384, 83348, 83392, 83386, 83355, 83397, 83339)

EstadoFacId Descripcion	            TipoFacId
12	        Problema con el NIT	    6
13	        Problema con el Email	6
14	        Problema de Sistema	    6
15	        Problema con Libelula	6
16	        Excepcion desconocida	6


Scaffold-DbContext "Server=tcp:192.168.3.133,1433;Database=ADMIN_EMPRESA;Trusted_Connection=True;" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables fo_extracto_BMSC_99,fo_extracto_BMSC_202101,fo_extracto_BUNION_99,fo_extracto_BUNION_202101,fo_extracto_BCP_99,fo_extracto_BCP,auxiliar_asistencia_99,ro_ControlAsistencia

## Comando para refacturar
Para poner nuevamente en cola por casos:
```
SELECT *
FROM dbo.FacturaLibelulaEvento
WHERE Mensaje LIKE 'Empresa bajo la nueva modalidad de facturación SFE: códigos de producto no registrados%'

UPDATE dbo.ao_ventas_cobranza_fac_ONLINE
SET EstadoFacId = 1
WHERE IdFactura IN (
	SELECT IdFactura
	FROM dbo.FacturaLibelulaEvento
	WHERE Mensaje LIKE 'Empresa bajo la nueva modalidad de facturación SFE: códigos de producto no registrados%'
)
```
Punto de corte:
```
UPDATE dbo.ao_ventas_cobranza_fac
SET EstadoFacId = 23
WHERE estado_codigo_fac IN ('APR', 'ANL', 'ERR')

UPDATE dbo.ao_ventas_cobranza_fac
SET EstadoFacId = 0
WHERE estado_codigo_fac = 'REG'
```
Verificacion de facturas aprobadas en dia de corte
```
DECLARE @FechaActual DATE = GETDATE()

SELECT *
FROM dbo.ao_ventas_cobranza_fac
WHERE estado_codigo_fac = 'APR'
AND YEAR(fecha_fac) = YEAR(@FechaActual)
AND MONTH(fecha_fac) = MONTH(@FechaActual)
AND DAY(fecha_fac) = DAY(@FechaActual)

SELECT *
FROM dbo.ao_ventas_cobranza_fac
WHERE estado_codigo_fac = 'REG'
AND YEAR(fecha_registro) = YEAR(@FechaActual)
AND MONTH(fecha_registro) = MONTH(@FechaActual)
AND DAY(fecha_registro) = DAY(@FechaActual)
```
Conectarse a contenedor:
```
docker attach --sig-proxy=false name-contenedor
```