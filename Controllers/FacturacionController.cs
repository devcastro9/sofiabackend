﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SofiaBackend.Data;
using SofiaBackend.Services;
using SofiaBackend.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SofiaBackend.Controllers
{
    [Route("f2/[controller]")]
    [ApiController]
    public class FacturacionController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly LibelulaApi _libelula;
        private readonly ITestInternet _internet;
        private readonly ISiatFacturacion _siat;
        private readonly IDataFacturacion _dataFacturacion;
        private readonly ILogger<FacturacionController> _logger;
        public FacturacionController(AppDbContext context, LibelulaApi libelula, ITestInternet internet, ISiatFacturacion siat, IDataFacturacion dataFacturacion, ILogger<FacturacionController> logger)
        {
            _context = context;
            _libelula = libelula;
            _internet = internet;
            _siat = siat;
            _dataFacturacion = dataFacturacion;
            _logger = logger;
        }
        // GET: f2/Facturacion/12
        [HttpGet("{id}")]
        public async Task<ActionResult<Factura?>> CallbackUrl(long id)
        {
            _logger.LogInformation("Callback");
            Factura? result = await _dataFacturacion.GetData(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
        // GET: f2/Facturacion/Conexion
        [HttpGet("Conexion")]
        public async Task<ActionResult<EstadosConexion>> GetEstadoConexion()
        {
            try
            {
                string? Appkey = await (from c in _context.FacConfiguraciones where c.Estado select c.AppKey).FirstOrDefaultAsync();
                string? TokenSiat = await (from c in _context.FacConfiguraciones where c.Estado select c.TokenDelegado).FirstOrDefaultAsync();
                EstadosConexion estado = new()
                {
                    Internet = await _internet.ExisteInternet(),
                    Impuestos = await _siat.VerificarComunicacionSiat(TokenSiat),
                    Libelula = await _libelula.EsOnline(Appkey)
                };
                return Ok(estado);
            }
            catch (Exception)
            {
                return BadRequest(new EstadosConexion() { Internet = false, Impuestos = false, Libelula = false });
            }
        }
    }
}
