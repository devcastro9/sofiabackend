﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaBackend.Models;

[Table("gc_beneficiario")]
public partial class GcBeneficiario
{
    [Key]
    [Column("beneficiario_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string BeneficiarioCodigo { get; set; } = null!;

    [Column("depto_sigla")]
    [StringLength(10)]
    [Unicode(false)]
    public string? DeptoSigla { get; set; }

    [Column("beneficiario_iniciales")]
    [StringLength(10)]
    [Unicode(false)]
    public string? BeneficiarioIniciales { get; set; }

    [Column("tipodoc_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? TipodocCodigo { get; set; }

    [Column("tipoben_codigo")]
    public int? TipobenCodigo { get; set; }

    [Column("beneficiario_nit")]
    [StringLength(20)]
    [Unicode(false)]
    public string? BeneficiarioNit { get; set; }

    [Column("beneficiario_primer_apellido")]
    [StringLength(35)]
    [Unicode(false)]
    public string? BeneficiarioPrimerApellido { get; set; }

    [Column("beneficiario_segundo_apellido")]
    [StringLength(30)]
    [Unicode(false)]
    public string? BeneficiarioSegundoApellido { get; set; }

    [Column("beneficiario_nombres")]
    [StringLength(30)]
    [Unicode(false)]
    public string? BeneficiarioNombres { get; set; }

    [Column("beneficiario_denominacion")]
    [StringLength(100)]
    [Unicode(false)]
    public string? BeneficiarioDenominacion { get; set; }

    [Column("beneficiario_fecha_nacimiento", TypeName = "datetime")]
    public DateTime? BeneficiarioFechaNacimiento { get; set; }

    [Column("beneficiario_telefono_fijo")]
    [StringLength(30)]
    [Unicode(false)]
    public string? BeneficiarioTelefonoFijo { get; set; }

    [Column("beneficiario_telefono_Of")]
    [StringLength(30)]
    [Unicode(false)]
    public string? BeneficiarioTelefonoOf { get; set; }

    [Column("beneficiario_telefono_Cel")]
    [StringLength(30)]
    [Unicode(false)]
    public string? BeneficiarioTelefonoCel { get; set; }

    [Column("beneficiario_email")]
    [StringLength(80)]
    [Unicode(false)]
    public string? BeneficiarioEmail { get; set; }

    [Column("beneficiario_email_of")]
    [StringLength(80)]
    [Unicode(false)]
    public string? BeneficiarioEmailOf { get; set; }

    [Column("beneficiario_domicilio_legal")]
    [StringLength(180)]
    [Unicode(false)]
    public string? BeneficiarioDomicilioLegal { get; set; }

    [Column("pais_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? PaisCodigo { get; set; }

    [Column("depto_codigo")]
    [StringLength(2)]
    [Unicode(false)]
    public string? DeptoCodigo { get; set; }

    [Column("prov_codigo")]
    [StringLength(4)]
    [Unicode(false)]
    public string? ProvCodigo { get; set; }

    [Column("munic_codigo")]
    [StringLength(9)]
    [Unicode(false)]
    public string? MunicCodigo { get; set; }

    [Column("comun_codigo")]
    [StringLength(50)]
    [Unicode(false)]
    public string? ComunCodigo { get; set; }

    [Column("beneficiario_edif_nro")]
    [StringLength(10)]
    [Unicode(false)]
    public string? BeneficiarioEdifNro { get; set; }

    [Column("beneficiario_edif_piso_nro")]
    [StringLength(10)]
    [Unicode(false)]
    public string? BeneficiarioEdifPisoNro { get; set; }

    [Column("beneficiario_edif_depto_nro")]
    [StringLength(10)]
    [Unicode(false)]
    public string? BeneficiarioEdifDeptoNro { get; set; }

    [Column("zona_codigo")]
    public int? ZonaCodigo { get; set; }

    [Column("calle_codigo")]
    public int? CalleCodigo { get; set; }

    [Column("edif_codigo")]
    [StringLength(21)]
    [Unicode(false)]
    public string? EdifCodigo { get; set; }

    [Column("beneficiario_id")]
    public int? BeneficiarioId { get; set; }

    [Column("beneficiario_deudor")]
    [StringLength(2)]
    [Unicode(false)]
    public string? BeneficiarioDeudor { get; set; }

    [Column("cargo_puesto")]
    [StringLength(100)]
    [Unicode(false)]
    public string? CargoPuesto { get; set; }

    [Column("codigo_corto")]
    [StringLength(5)]
    [Unicode(false)]
    public string? CodigoCorto { get; set; }

    [Column("estado_codigo_contrato")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigoContrato { get; set; }

    [Column("estado_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigo { get; set; }

    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }

    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigo { get; set; }

    [Column("fecha_aprueba", TypeName = "datetime")]
    public DateTime? FechaAprueba { get; set; }

    [Column("usr_codigo_aprueba")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigoAprueba { get; set; }

    [Column("razon_social_sin")]
    [StringLength(200)]
    [Unicode(false)]
    public string? RazonSocialSin { get; set; }

    [Column("analizado")]
    public bool? Analizado { get; set; }

    [Column("cola_revision")]
    public bool? ColaRevision { get; set; }

    [Column("valido_sin")]
    public bool? ValidoSin { get; set; }

    [Column("activo_sin")]
    public bool? ActivoSin { get; set; }
}
