﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaBackend.Models;

[Keyless]
public partial class FacturaElecDetalle
{
    public long IdFactura { get; set; }

    [Column("actividad")]
    public int? Actividad { get; set; }

    [Column("producto_sin")]
    public int? ProductoSin { get; set; }

    [Column("producto")]
    [StringLength(25)]
    [Unicode(false)]
    public string? Producto { get; set; }

    [Column("concepto")]
    [StringLength(300)]
    [Unicode(false)]
    public string? Concepto { get; set; }

    public int Cantidad { get; set; }

    public int UnidadMedida { get; set; }

    [Column("costo", TypeName = "decimal(18, 2)")]
    public decimal? Costo { get; set; }
}
